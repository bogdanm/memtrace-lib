import os, sys
import struct
import argparse

def write_string_with_tag(f, tag, s):
    """ Write an ASCIIZ string, preceeded by a tag (byte) and string length (4 bytes) """
    f.write(struct.pack("<BI", tag, len(s)))
    f.write(s)
    f.write(chr(0))

# Parse options
parser = argparse.ArgumentParser(description = "Run the memory tracer")
parser.add_argument('-i', dest = 'interpreter', help = 'Lua interpreter to use', required = True)
parser.add_argument('-s', dest = 'script', help = 'Lua script to run', required = True)
parser.add_argument('-o', dest = 'output', help = 'Output file name (default: script name + ".memtrace")', required = False)
parser.add_argument('-t', dest = 'stacktrace', help = 'Add a stack trace to each call', required = False, default = True, action = 'store_true')
parser.add_argument('-y', dest = 'overwrite', help = 'Overwrite output file if it exists', required = False, default = False, action = 'store_true')
args = vars(parser.parse_args())

lua_executable, script_name = os.path.abspath(args['interpreter']), os.path.abspath(args['script'])
if not os.path.isfile(lua_executable):
    print "File '%s' not found" % lua_executable
    os._exit(1)
if not os.path.isfile(script_name):
    print "File '%s' not found" % script_name
    os._exit(1)
output_name = args['output'] or (script_name + '.memtrace')
exec_marker, script_marker = 10, 11
if os.path.isfile(output_name) and not args['overwrite']:
    print "Output file '%s' already exists (use '-y' to overwrite)" % output_name
    os._exit(1)

# Write header (interpreter name and script name)
with open(output_name, "wb") as f:
    write_string_with_tag(f, exec_marker, lua_executable)
    write_string_with_tag(f, script_marker, script_name)

# Run script with intercept.so in LD_PRELOAD
if args["stacktrace"]:
    os.environ["MEMTRACE_OPTIONS"] = "stacktrace"
os.environ["MEMTRACE_OUTPUT_FILE"] = output_name
os.system("LD_PRELOAD='%s' %s %s" % (os.environ["INTERCEPT_LD_PATH"], lua_executable,  script_name))
