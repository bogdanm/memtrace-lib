import struct
import sys
from elftools.elf.sections import SymbolTableSection
from elftools.elf.elffile import ELFFile
import bisect

# Decode a stack trace, translating addresses to function names
class StackTraceDecoder(object):

    FUNCNAME_IDX, ADDRESS_IDX, OFFSET_IDX = range(3)

    def __init__(self, fname):
        self.f = open(fname, "rb")
        self.elf = ELFFile(self.f)
        self.read_symbols()

    def read_symbols(self):
        """ Iterates thorugh all symbols of type FUNC in the elf file,
            building a list of (function name, start address)"""
        self.symbols = []
        for section in self.elf.iter_sections():
            if not isinstance(section, SymbolTableSection):
                continue
            if section['sh_entsize'] == 0:
                continue
            for nsym, symbol in enumerate(section.iter_symbols()):
                if symbol['st_info']['type'] == 'STT_FUNC':
                    self.symbols.append((symbol.name, symbol['st_value']))
        self.symbols.sort(key = lambda e: e[1])
        self.minaddr, self.maxaddr = self.symbols[0][1], self.symbols[-1][1]
        self.addrs = [e[1] for e in self.symbols]
    
    def resolve_address(self, addr):
        """ Translates the given address into a (func_name, address, address_offset_from_function_start) tuple"""
        if addr < self.minaddr or addr > self.maxaddr:
            return ('??', addr, 0)
        i = bisect.bisect_left(self.addrs, addr)
        if addr < self.addrs[i]:
            i = i - 1
        return (self.symbols[i][0], addr, addr - self.symbols[i][1])

    def resolve_trace(self, trace):
        """ Translates a complete stack trace """
        return [self.resolve_address(addr) for addr in trace]

# Read a memtrace file, according to the format defined by memtrace.c
class MemtraceReader(object):

    # These correspond to the values in memtrace.c
    (OP_MALLOC, OP_FREE, OP_REALLOC, OP_CALLOC) = range(4)
    # These correspond to the values in runtrace.py
    EXEC_MARKER, SCRIPT_MARKER = 10, 11

    def __init__(self, tracefile, interpret_realloc = False):
        self.f = open(tracefile, "rb")
        self.decoders = {
            self.OP_MALLOC: self.decode_malloc,
            self.OP_FREE: self.decode_free,
            self.OP_REALLOC: self.decode_realloc,
            self.OP_CALLOC: self.decode_calloc
        }
        self.interpret_realloc = interpret_realloc
        self.lua_executable, self.lua_script = None, None
        self.decode_header()

    def __iter__(self):
        return self

    def get_lua_executable(self):
        return self.lua_executable

    def get_lua_script(self):
        return self.lua_script

    def decode_header(self):
        """ Decodes the header, as defined in runtrace.py. The header is optional """
        while True:
            tag = struct.unpack_from("B", self.f.read(1))[0]
            if tag != self.EXEC_MARKER and tag != self.SCRIPT_MARKER:
                self.f.seek(-1, 1)
                break
            l = struct.unpack_from("<I", self.f.read(4))[0]
            data = struct.unpack_from("%ss" % l, self.f.read(l + 1))[0]
            if tag == self.EXEC_MARKER:
                self.lua_executable = data
            else:
                self.lua_script = data

    def decode_malloc(self):
        """ Decode a malloc call (parameters: pointer to allocated memory and size) """
        op = struct.unpack_from("<2I", self.f.read(8))
        return {"type": "malloc", "pointer": op[0], "size": op[1]}

    def decode_realloc(self):
        """ Decode a realloc call. If interpret_realloc is set, it will check if the
            realloc operation is actually a malloc or a free """
        op = struct.unpack_from("<3I", self.f.read(12))
        result, original_pointer, new_size = op[0], op[1], op[2]
        if self.interpret_realloc:
            # Realloc can also mean malloc/free
            if original_pointer == 0:
                return {"type": "malloc", "pointer": result, "size": new_size}
            elif new_size == 0:
                return {"type": "free", "pointer": original_pointer}
        return {"type": "realloc", "pointer": result, "original_pointer": original_pointer, "new_size": new_size}

    def decode_free(self):
        """ Decodes a free call (parameters: pointer to free) """
        op = struct.unpack_from("<I", self.f.read(4))
        return {"type": "free", "pointer": op[0]}
    
    def decode_calloc(self):
        """ Decodes a calloc call (parameters: pointer to allocated memory, number of elements, element size) """
        op = struct.unpack_from("<2I", self.f.read(12))
        return {"type": "calloc", "pointer": op[0], "number_of_elements": op[1], "element_size": op[2]}

    def decode_stack_trace(self):
        """ Decodes a stack trace (list of program counters, ends with a 0) """
        stack = []
        while True:
            ip = struct.unpack_from("<I", self.f.read(4))[0]
            if ip == 0:
                break
            stack.append(ip)
        return stack

    def next(self):
        """ Return the next encoded operation """
        try:
            op = struct.unpack_from("<I", self.f.read(4))[0]
        except:
            raise StopIteration
        try:
            if op in self.decoders.keys():
                data = self.decoders[op]()
                trace_type = struct.unpack_from("<I", self.f.read(4))[0]
                data["stacktrace"] = self.decode_stack_trace() if trace_type else None
                return data
            else:
                raise Exception("Invalid operation")
        except:
            raise Exception("Invalid file format")

def runme():
    o = MemtraceReader(sys.argv[1], True)
    if o.get_lua_executable():
        d = StackTraceDecoder(o.get_lua_executable())
    else:
        d = None
    maxmem, crtmem, reallocs, frees, allocs = 0, 0, 0, 0, 0
    counters = {"malloc": 0, "realloc": 0, "free": 0, "calloc": 0}
    sizemap = {}
    calls = None
    for entry in o:
        sizemap[0] = 0
        t = entry["type"]
        counters[t] = counters[t] + 1
        if t == "malloc":
            crtmem = crtmem + entry["size"]
            sizemap[entry["pointer"]] = entry["size"]
        elif t == "free":
            p = entry["pointer"]
            crtmem = crtmem - sizemap[p]
            del sizemap[p]
        elif t == "realloc":
            crtmem = crtmem - sizemap[entry["original_pointer"]] + entry["new_size"]
            del sizemap[entry["original_pointer"]]
            sizemap[entry["pointer"]] = entry["new_size"]
        if crtmem > maxmem:
            maxmem = crtmem
            calls = entry["stacktrace"]
    print "Analysis for %s" % o.get_lua_script()
    print counters, maxmem, crtmem
    if d:
        for e in d.resolve_trace(calls):
            print "[%08X] %s + 0x%X" % (e[d.ADDRESS_IDX], e[d.FUNCNAME_IDX], e[d.OFFSET_IDX])

if __name__ == "__main__":
    runme()
