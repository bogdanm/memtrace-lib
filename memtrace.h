#ifndef __MEMTRACE_H__
#define __MEMTRACE_H__

#include <stdio.h>

// Error codes
enum {
    MEMTRACE_OK,
    MEMTRACE_EOF,
    MEMTRACE_NOT_INITIALIZED,
    MEMTRACE_IO_ERROR,
    MEMTRACE_INVALID_OP
};

// Initialization options
#define MEMTRACE_OPT_STACKTRACE               1

void memtrace_init(FILE *fp, int opts);
int memtrace_add_malloc(void *result, size_t size);
int memtrace_add_realloc(void *result, void *p, size_t size);
int memtrace_add_free(void *p);
int memtrace_add_calloc(void *result, size_t nmemb, size_t size);

#endif

