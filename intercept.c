#define _GNU_SOURCE

#include <dlfcn.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include "memtrace.h"

/******************************************************************************/
// Internal data and functions

typedef void* (*p_realloc)(void*, size_t);
typedef void (*p_free)(void*);
typedef void* (*p_malloc)(size_t);
typedef void* (*p_calloc)(size_t, size_t);

static p_realloc real_realloc = NULL;
static p_free real_free = NULL;
static p_malloc real_malloc = NULL;
static p_calloc real_calloc = NULL;

static volatile int forbid_tracing;
static FILE* memtrace_fp = NULL;

#define DEFAULT_OUTPUT_FILE                             "memtrace.out"

// Initialize the output file in the constructor
// Reads the file name from the environment variable MEMTRACE_OUTPUT_FILE
// If not found, defaults to DEFAULT_OUTPUT_FILE
static void tracerh_init(void) __attribute__((constructor));
static void tracerh_init(void) {
    const char *fname, *opts;
    int nopts = 0;

    if((fname = getenv("MEMTRACE_OUTPUT_FILE")) == NULL)
        fname = DEFAULT_OUTPUT_FILE;
    if((opts = getenv("MEMTRACE_OPTIONS")) != NULL) {
        if(!strcmp(opts, "stacktrace"))
            nopts |= MEMTRACE_OPT_STACKTRACE;
    }
    if((memtrace_fp = fopen(fname, "r+b")) == NULL) {
        if((memtrace_fp = fopen(fname, "wb")) == NULL) {
            fprintf(stderr, "memtrace: unable to open file '%s' for writing", fname);
            exit(1);
        }
    } else {
        fseek(memtrace_fp, 0, SEEK_END);
    }
    memtrace_init(memtrace_fp, nopts);
}

// Close the file descriptor on exit
static void tracerh_deinit(void) __attribute__((destructor));
static void tracerh_deinit(void) {
    forbid_tracing = 1;
    if(memtrace_fp != NULL)
        fclose(memtrace_fp);
}

// Save pointers to the real implementations of traced functions
static void tracerh_get_functions(void)
{
    real_malloc = (p_malloc)dlsym(RTLD_NEXT, "malloc");
    real_realloc = (p_realloc)dlsym(RTLD_NEXT, "realloc");
    real_free = (p_free)dlsym(RTLD_NEXT, "free");
    real_calloc = (p_calloc)dlsym(RTLD_NEXT, "calloc");
}

/******************************************************************************/
// Public interface

void *malloc(size_t size) {
    void *res;

    if(NULL == real_malloc)
        tracerh_get_functions();
    if(forbid_tracing)
        return real_malloc(size);
    forbid_tracing = 1;
    res = real_malloc(size);
    memtrace_add_malloc(res, size);
    forbid_tracing = 0;
    return res;
}

void *realloc(void *p, size_t size) {
    void *newp;

    if(NULL == real_realloc)
        tracerh_get_functions();
    if(forbid_tracing)
        return real_realloc(p, size);
    forbid_tracing = 1;
    newp = real_realloc(p, size);
    memtrace_add_realloc(newp, p, size);
    forbid_tracing = 0;
    return newp;
}

void free(void *p) {
    if(NULL == real_free)
        tracerh_get_functions();
    if(forbid_tracing) {
        real_free(p);
        return;
    }
    forbid_tracing = 1;
    real_free(p);
    memtrace_add_free(p);
    forbid_tracing = 0;
}

void *calloc(size_t nmemb, size_t size) {
    void *res;

    if (NULL == real_calloc)
        tracerh_get_functions();
    if (forbid_tracing)
        return real_calloc(nmemb, size);
    forbid_tracing = 1;
    res = real_calloc(nmemb, size);
    memtrace_add_calloc(res, nmemb, size);
    forbid_tracing = 0;
    return res;
}

