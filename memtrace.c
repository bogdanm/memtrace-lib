#include <stdio.h>
#include <stdarg.h>
#include <sys/types.h>
#include <unistd.h>
#define UNW_LOCAL_ONLY
#include <libunwind.h>
#include <string.h>
#include <stdlib.h>
#include "memtrace.h"

/*****************************************************************************/
// Internal data

// Memory operations
enum {
    MEMTRACE_OP_MALLOC,
    MEMTRACE_OP_FREE,
    MEMTRACE_OP_REALLOC,
    MEMTRACE_OP_CALLOC
};

static FILE* memtrace_fp = NULL;
static int memtrace_opts = 0;

/*****************************************************************************/
// Internal functions

static int memtraceh_write32(unsigned int data) {
    return fwrite(&data, 4, 1, memtrace_fp) == 1;
}

// Write a stacktrace (obtained using libunwind) in the destination file
// Each entry has a 4 byte IP. Entries end with NULL (0).
static int memtraceh_write_stacktrace(void) {
    unw_cursor_t cursor;
    unw_context_t uc;
    unw_word_t ip;

    unw_getcontext(&uc);
    unw_init_local(&cursor, &uc);
    while(unw_step(&cursor) > 0) {
        unw_get_reg(&cursor, UNW_REG_IP, &ip);
        memtraceh_write32((unsigned int)ip);
    }
    // Write terminator: single int (0)
    memtraceh_write32(0);
    return MEMTRACE_OK;
}

static int memtraceh_add_entry(int type, ...) {
    va_list va;

    if(NULL == memtrace_fp)
        return MEMTRACE_NOT_INITIALIZED;
    va_start(va, type);
    memtraceh_write32(type);
    switch(type) {
        case MEMTRACE_OP_MALLOC:
            memtraceh_write32((unsigned int)va_arg(va, void*)); // result
            memtraceh_write32((unsigned int)va_arg(va, size_t)); // requested size
            break;

        case MEMTRACE_OP_REALLOC:
            memtraceh_write32((unsigned int)va_arg(va, void*)); // result
            memtraceh_write32((unsigned int)va_arg(va, void*)); // original pointer
            memtraceh_write32((unsigned int)va_arg(va, size_t)); // new size
            break;

        case MEMTRACE_OP_FREE:
            memtraceh_write32((unsigned int)va_arg(va, void*)); // original pointer
            break;

        case MEMTRACE_OP_CALLOC:
            memtraceh_write32((unsigned int)va_arg(va, void*)); // result
            memtraceh_write32((unsigned int)va_arg(va, size_t)); // number of elements
            memtraceh_write32((unsigned int)va_arg(va, size_t)); // element size
            break;

        default:
            va_end(va);
            return MEMTRACE_INVALID_OP;
    }
    // Add a stacktrace if needed
    if((memtrace_opts & MEMTRACE_OPT_STACKTRACE) != 0) {
        memtraceh_write32(1);
        memtraceh_write_stacktrace();
    } else {
        memtraceh_write32(0);
    }
    va_end(va);
    return MEMTRACE_OK;
}

/*****************************************************************************/
// Public interface

void memtrace_init(FILE *fp, int opts) {
    memtrace_fp = fp;
    memtrace_opts = opts;
}

int memtrace_add_malloc(void *result, size_t size) {
    return memtraceh_add_entry(MEMTRACE_OP_MALLOC, result, size);
}

int memtrace_add_realloc(void *result, void *p, size_t size) {
    return memtraceh_add_entry(MEMTRACE_OP_REALLOC, result, p, size);
}

int memtrace_add_free(void *p) {
    return memtraceh_add_entry(MEMTRACE_OP_FREE, p);
}

int memtrace_add_calloc(void *result, size_t nmemb, size_t size) {
    return memtraceh_add_entry(MEMTRACE_OP_CALLOC, result, nmemb, size);
}


