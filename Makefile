CC             = gcc
OBJ_CFLAGS     = -std=gnu99 -m32 -O2 -Wall -fPIC
TARGET_CFLAGS  = -m32 -shared
LIBS           = -ldl -lunwind
SOURCES        = intercept.c memtrace.c
OBJECTS        = $(SOURCES:.c=.o)
TARGET         = intercept.so

all: $(TARGET)
 
$(TARGET): $(OBJECTS)
	$(CC) $(TARGET_CFLAGS) -o $(TARGET) $(OBJECTS) $(LIBS)
 
clean:
	-rm -f $(OBJECTS)
	-rm -f $(TARGET)
  
%.o: %.c
	$(CC) $(OBJ_CFLAGS) -c -o $@ $<
 
.PHONY : all clean

memtrace.o: memtrace.c memtrace.h
intercept.o: intercept.c memtrace.h